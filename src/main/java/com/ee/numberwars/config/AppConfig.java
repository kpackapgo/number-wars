package com.ee.numberwars.config;

import com.ee.numberwars.model.GameStepDTO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class  AppConfig {

    @Bean
    public Sinks.Many<GameStepDTO> sink(){
        return Sinks.many().replay().latest();
    }

    @Bean
    public Flux<GameStepDTO> flux(Sinks.Many<GameStepDTO> sink){
        return sink.asFlux();
    }

    @Bean
    public Map<Integer, List<Integer>> dividerAndSteps() {
        Map<Integer, List<Integer>> dividerAndSteps = new HashMap<>();
        dividerAndSteps.put(5, List.of(-2, -1, 0, 1, 2));
        dividerAndSteps.put(7, List.of(-3, -2, -1, 0, 1, 2, 3));
        dividerAndSteps.put(11, List.of(-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5));
        dividerAndSteps.put(13, List.of(-6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6));
        return dividerAndSteps;
    }
}
