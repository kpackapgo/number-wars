package com.ee.numberwars.service;

import com.ee.numberwars.model.GameStepDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.*;

@Service
public class GameServiceImpl implements GameService{

    private List<String> queuedUsers = new ArrayList<>();
    private final int[] dividers = {5,7,11,13};
    private Map<Integer, List<Integer>> dividerAndSteps;
    private Game game;

    private Sinks.Many<GameStepDTO> sink;

    @Autowired
    public GameServiceImpl(Map<Integer, List<Integer>> dividerAndSteps, Sinks.Many<GameStepDTO> sink) {
        this.dividerAndSteps = dividerAndSteps;
        this.sink = sink;
    }

    @Override
    @Scheduled(fixedRate = 5000)
    public void setUpGame() {
        System.out.println("Queue size is: " + queuedUsers.size());
        if(queuedUsers.size() == 2) {

            //TODO: this can be later refactored to work for more than two people
            System.out.println("STARTING GAME");
            game = new Game();
            game.addPlayers(queuedUsers.get(0), queuedUsers.get(1));

            GameStepDTO gameStepDTO = initGame();
            gameStepDTO.setCurrentUser(determineStartingUser(queuedUsers));

            queuedUsers.clear();
            Flux.just(gameStepDTO).subscribe(this.sink::tryEmitNext);
        }
    }

    @Override
    public void queuePlayer(String username) {
        queuedUsers.add(username);
    }


    private GameStepDTO initGame() {
        GameStepDTO gameStep = new GameStepDTO();
        int playNumber = generatePlayNumber(2000, 10000);
        int divider = getDivider();
        List<Integer> steps = dividerAndSteps.get(divider);
        gameStep.setNumber(playNumber);
        gameStep.setDivider(divider);
        gameStep.setSteps(steps);

        game.setDivider(divider);
        game.setNumber(playNumber);
        game.setSteps(steps);
        return gameStep;
    }

    @Override
    public void executeStep(String currentUser, int step) {

        GameStepDTO gameStepDTO = new GameStepDTO();

        //evaluate the new number that has to be send to the client
        int newNumber = (game.getNumber()+step)/game.getDivider();
        gameStepDTO.setNumber(newNumber);
        game.setNumber(newNumber);
        gameStepDTO.setStep(step);
        gameStepDTO.setCurrentUser(game.getNextUser(currentUser));
        Flux.just(gameStepDTO).subscribe(this.sink::tryEmitNext);
    }

    private int getDivider() {
        int rnd = new Random().nextInt(dividers.length);
        return dividers[rnd];
    }

    private int generatePlayNumber(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    private String determineStartingUser(List<String> users) {
        Random rand = new Random();
        return users.get(rand.nextInt(users.size()));
    }
}
