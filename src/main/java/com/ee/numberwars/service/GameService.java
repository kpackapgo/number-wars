package com.ee.numberwars.service;

public interface GameService {

    void setUpGame();

    void queuePlayer(String username);

    void executeStep(String previousUser, int step);
}
