package com.ee.numberwars.service;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Setter
@Getter
public class Game {

    Map<String, String> usersPlaying = new HashMap<>();
    int number;
    int divider;
    List<Integer> steps;

    public String getNextUser(String previousUser) {
        return usersPlaying.get(previousUser);
    }

    public void addPlayers(String user1, String user2) {
        usersPlaying.put(user1, user2);
        usersPlaying.put(user2, user1);
    }
}
