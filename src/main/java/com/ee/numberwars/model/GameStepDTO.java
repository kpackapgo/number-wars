package com.ee.numberwars.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GameStepDTO implements Serializable {

    private int number;
    private int lastStep;
    private int divider;
    private List<Integer> steps;
    private int step;
    private String currentUser;
}
