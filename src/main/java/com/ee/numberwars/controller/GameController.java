package com.ee.numberwars.controller;

import com.ee.numberwars.service.GameService;
import com.ee.numberwars.model.GameStepDTO;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

@Controller
public class GameController {

    private GameService gameService;
    private Flux<GameStepDTO> flux;

    public GameController(GameService gameService, Flux<GameStepDTO> flux) {
        this.gameService = gameService;
        this.flux = flux;
    }

    @GetMapping("/login")
    public String goToLogin() {
        return "gamequeue";
    }

    @PostMapping("/join")
    public String process(Model model, @RequestParam String username) {
        gameService.queuePlayer(username);
        model.addAttribute("userName", username);
        return "game";
    }

    @GetMapping(value = "/start", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<GameStepDTO> goToGame() {
        return flux;
    }

    @PostMapping("/step")
    @ResponseBody
    public void executeStep(@RequestBody GameStepDTO step) {
        gameService.executeStep(step.getCurrentUser(), step.getStep());
    }
}
