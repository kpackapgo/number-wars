APPLICATION START WITH DOCKER
=======================================
Start the project with these steps:

1. Build jar with this command:

mvnw package && java -jar target/numberwars.jar

2. Create docker image with this command:

docker build -t numberwars/numberwars-docker .

3. Run docker container with this command:

docker run -p 8080:8080 numberwars/numberwars-docker

4. If you want to stop the application
NOTE: will stopp all containers
docker stop $(docker ps -aq)

CASE OF NO DOCKER:
======================================
If for some reason this is not working you can start the spring app directly from the NUmberWarsApplication class


